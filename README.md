# Open Source for Good
A list of open source projects and organizations trying to fix our broken world.

## [BrasilIO](https://github.com/turicas/brasil.io)
Brazil with data freedom. Repository of public data made available in an accessible format.

## [CoopCycle](https://github.com/coopcycle)
CoopCycle is a federation of bike delivery co-ops. Governed democratically by coops, it enables them to stand united and to reduce their costs thanks to resources pooling. It creates a strong bargaining power to protect the bikers rights. 

## [Data for Democracy](https://github.com/Data4Democracy)
A community of data people volunteering on social impact projects.

## [HospitalRun](https://github.com/hospitalrun/)
Open source, modern software for charitable hospitals in the developing world.

## [Humanitarian Toolbox](http://www.htbox.org/)
The Humanitarian Toolbox is a sustained effort to leverage technology and skilled volunteer communities to solve the needs of response organizations and communities affected by disasters.

### [allReady](https://github.com/HTBox/allReady)
This repo contains the code for allReady, an open-source solution focused on increasing awareness, efficiency and impact of preparedness campaigns as they are delivered by humanitarian and disaster response organizations in local communities.

### [Two Weeks Ready](https://github.com/HTBox/TwoWeeksReady)
For many years, we’ve been talking about the importance of being prepared for 72 hours. This is a good start, and helpful in the event of short-term power outages or temporary evacuation.

## [Karrot](https://karrot.world)
Karrot is a free and open-source tool for grassroots initiatives and groups of people that want to coordinate face-to-face activities on a local, autonomous and voluntary basis. It is designed in ways to enable community-building and support a more transparent, democratic and participatory governance of your group.

### [Karrot Frontend](https://github.com/yunity/karrot-frontend)
Web application for organization of foodsaving groups worldwide - frontend code and central location for feature planning.

### [Karrot Backend](https://github.com/yunity/karrot-backend)
Django API server for the karrot frontend.

## [Mifos](https://github.com/openMF/)
Mifos X is an extended platform for delivering the complete range of financial services needed for an effective financial inclusion solution.

## [Open Food Network](https://github.com/openfoodfoundation)
The Open Food Network is a global network of people and organisations working together to build a new food system. Together, we develop open and shared resources, knowledge and software to support a better food system.

## [Open Knowledge Foundation](https://github.com/okfn)
Our mission is to create a more open world – a world where all non-personal information is open, free for everyone to use, build on and share; and creators and innovators are fairly recognised and rewarded.

## [Terrastories](https://github.com/terrastories/terrastories) 
Terrastories is a geostorytelling application built to enable indigenous and other local communities to locate and map their own oral storytelling traditions about places of significant meaning or value to them. Community members can add places and stories through a user-friendly interface, and make decisions about designating certain stories as private or restricted.

## Other Lists
I am still curating the list below.

- [Make a social impact by contributing to these open source projects](https://towardsdatascience.com/make-a-social-impact-by-contributing-to-these-open-source-projects-1d6d34e2b8b1)
- [33 High Impact Open Source Projects Seeking Contributors](https://www.nexmo.com/legacy-blog/2020/10/16/33-high-impact-open-source-projects-seeking-contributors)
- [Five Open Source Projects We Love](https://casefoundation.org/blog/five-open-source-projects-we-love/)
- [GitHub Social Impact](https://socialimpact.github.com/)
- [HFOSS Projects](http://www.foss2serve.org/index.php/HFOSS_Projects)
- [Open Knowledge Brasil - Rede pelo Conhecimento Livre](https://github.com/okfn-brasil)
- [Vegan Hacktivists](https://veganhacktivists.org/projects)